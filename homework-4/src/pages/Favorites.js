import React from 'react'
import ProductCard from '../componets/ProductCard';


function Favorites({favorites, addToCart, toggleFavorite}) {
  return (
    <div className="container">
    <h2 className="title-page">Favorites</h2>
    <ul className="card-list">{favorites.map((card) => (
        <ProductCard
          key={card.id}
          card={card}
          isFavorite={true}
          toggleFavorite={toggleFavorite}
          addToCart={addToCart}
        />
      ))}</ul>
  </div>
  );
}

export default Favorites
