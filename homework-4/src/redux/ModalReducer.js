import { SET_MODAL } from "./type";

const initialState = {
  isModalOpen: false,
  selectedItemId: null,
};

export const modalReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_MODAL:
      return {
        ...state,
        isModalOpen: action.data.isOpen,
        selectedItemId: action.data.id,
      };

    default:
      return state;
  }
};