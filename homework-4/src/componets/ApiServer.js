import axios from "axios";
import { setData } from "../redux/action";

const getData = () => {
  return async (dispatch) => {
    const response = await axios.get("products.json");
    dispatch(setData(response.data));
  };
};

export default getData;
