import React from "react";
import Button from "./componets/Button";
import Modal from "./componets/Modal";
import './scss/App.scss'
// function App() {
//   return (
//     <div>
//       <Button backgroundColor={"blue"} text={"Open first modal"} />
//       <Button backgroundColor={"blue"} text={"Open second modal"} />
//     </div>
//   );
// }

// export default App;

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstModalOpen: false,
      secondModalOpen: false
    };
  }

  openFirstModal = () => {
    this.setState({ firstModalOpen: true });
  };

  closeFirstModal = () => {
    this.setState({ firstModalOpen: false });
  };

  openSecondModal = () => {
    this.setState({ secondModalOpen: true });
  };

  closeSecondModal = () => {
    this.setState({ secondModalOpen: false });
  };

  render() {
    return (
      <div className="app">
        <Button
          backgroundColor="blue"
          text="Open first modal"
          onClick={this.openFirstModal}
        />
        <Button
          backgroundColor="green"
          text="Open second modal"
          onClick={this.openSecondModal}
        />

        {this.state.firstModalOpen && (
          <Modal
            header="Do you want to delete this file?"
            closeButton={true}
            text="Once you have deleted this file, it won't be possible to undo this action.
            A you should you want delete it ?"
            actions={
              <div>
                <button className="modal_btn" onClick={this.closeFirstModal}>Ok</button>
                <button className="modal_btn" onClick={this.closeFirstModal}>Close</button>
              </div>
            }
            onClose={this.closeFirstModal}
          />
        )}

        {this.state.secondModalOpen && (
          <Modal
            header="Second modal"
            closeButton={true}
            text="Good luck to you!!! "
            actions={
              <div>
              <button className="modal_btn"  onClick={this.closeSecondModal}>Close</button>
            </div>
            }
            onClose={this.closeSecondModal}
          />
        )}
      </div>
    );
  }
}

export default App;
