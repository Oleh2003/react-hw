import React from 'react'
import '../scss/Button.scss'


// export default function Button(props) {
//     const { backgroundColor, text } = props;
//    const [clicked , setCliked] = useState('')

//    function handelClick(){
//     setCliked(<Modal/>)
//    }
//   return (
//     <div>
//       <button onClick={handelClick} style={{backgroundColor: backgroundColor}} >{text}</button>
//       <div>{clicked}</div>
//     </div>
//   )
// }

class Button extends React.Component {
  render() {
    const { backgroundColor, text, onClick } = this.props;
    const buttonStyle = {
      backgroundColor: backgroundColor
    };
    return (
      <button className="button" style={buttonStyle} onClick={onClick}>
        {text}
      </button>
    );
  }
}

export default Button;