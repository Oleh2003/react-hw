import React from "react";
import '../scss/Modal.scss'

class Modal extends React.Component {
  closeModal = (e) => {
    if (e.target.classList.contains('modal')) {
      this.props.onClose();
    }
  };

  render() {
    const { header, closeButton, text, actions } = this.props;
    return (
      <div className="modal" onClick={this.closeModal}>
        <div className="modal_content">
          
          <div className="modal_block">
         <h2 className="modal_header">{header}</h2>
         {closeButton && (
            <span className="modal_closeButton" onClick={this.props.onClose}>
              &times;
            </span>
          )}
         </div>
          <p className="modal_text">{text}</p>
          <div className="modal_actions">{actions}</div>
        </div>
      </div>
    );
  }
}

export default Modal;
