import React from "react";
import PropTypes from "prop-types";
import "../scss/ProductsList.scss";
import ProductCart from "../componets/ProductCart";
import "../scss/Cart.scss";

const Cart = ({ cart, deleteFromCart, toggleFavorite, favorites }) => {
  const cartList = cart.map((card) => (
    <ProductCart
      key={card.id}
      card={card}
      deleteFromCart={deleteFromCart}
      toggleFavorite={toggleFavorite}
      isFavorite={favorites.some((p) => p.id === card.id)}
    />
  ));

  return (
    <div className="container">
      <h2 className="title-page">Cart</h2>
      <ul className="card-list">{cartList}</ul>
    </div>
  );
};

Cart.propTypes = {
  cart: PropTypes.arrayOf(PropTypes.object).isRequired,
  deleteFromCart: PropTypes.func.isRequired,
  toggleFavorite: PropTypes.func.isRequired,
  favorites: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default Cart;
