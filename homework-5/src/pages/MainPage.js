import React from 'react'
import ProductList from '../componets/ProductsList'

function MainPage({toggleFavorite, addToCart,cards,favorites}) {
  return (
     <div className="container">
 <ProductList
    toggleFavorite={toggleFavorite}
    addToCart={addToCart}
    cards={cards}
    favorites={favorites}
  />
</div> 
  )
}

export default MainPage
