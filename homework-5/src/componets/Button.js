import  React  from "react";
import PropTypes from "prop-types";
import "../scss/Button.scss";

function Button ({ backgroundColor, text, onClick }) {

    return (
      <button className="btn" style={{ backgroundColor }} onClick={onClick}>
        {text}
      </button>
    );
  }


Button.propTypes = {
  backgroundColor: PropTypes.string,
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default Button;