import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { BrowserRouter, Routes, Route, Link } from "react-router-dom";
import getData from "./componets/ApiServer";
import MainPage from "./pages/MainPage";
import Cart from "./pages/Cart";
import Favorites from "./pages/Favorites";
import "./scss/App.scss";

function App() {
  const [cards, setCards] = useState([]);
  const [cart, setCart] = useState([]);
  const [favorites, setFavorites] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const results = await getData();
      setCards(results);
    };

    fetchData();

    const savedCart = localStorage.getItem("cart");
    const savedFavorites = localStorage.getItem("favorites");

    if (savedCart) {
      setCart(JSON.parse(savedCart));
    }

    if (savedFavorites) {
      setFavorites(JSON.parse(savedFavorites));
    }
  }, []);

  useEffect(() => {
    localStorage.setItem("cart", JSON.stringify(cart));
    localStorage.setItem("favorites", JSON.stringify(favorites));
  }, [cart, favorites]);

  const addToCart = (product) => {
    setCart((prevCart) => [...prevCart, product]);
  };

  const addToFavorites = (product) => {
    setFavorites((prevFavorites) => [...prevFavorites, product]);
  };

  const removeFromFavorites = (card) => {
    setFavorites((prevFavorites) =>
      prevFavorites.filter((p) => p.id !== card.id)
    );
  };

  const toggleFavorite = (product) => {
    if (favorites.find((p) => p.id === product.id)) {
      removeFromFavorites(product);
    } else {
      addToFavorites(product);
    }
  };

  const deleteFromCart = (product) => {
    setCart((prevCart) => prevCart.filter((p) => p.id !== product.id));
  };

  

  return (
    <BrowserRouter>
      <div className="container">
        <div className="header">
          <Link to="/" className="header_title">
            Clothing Store
          </Link>
          <div className="header_btn-box">
            <Link to="/favorites" className="header_btn">
              Обране {favorites ? favorites.length : 0}
              &#9734;
            </Link>
            <Link to="/cart" className="header_btn">
              Кошик {cart ? cart.length : 0}
              &#128722;
            </Link>
          </div>
        </div>
      </div>
      <Routes>
        <Route
          path="/"
          element={
            <MainPage
              toggleFavorite={toggleFavorite}
              addToCart={addToCart}
              cards={cards}
              favorites={favorites}
            />
          }
        />
        <Route
          path="/cart"
          element={
            <Cart
              toggleFavorite={toggleFavorite}
              deleteFromCart={deleteFromCart}
              cart={cart}
              favorites={favorites}
            />
          }
        />
        <Route path="/favorites" element={<Favorites
        toggleFavorite={toggleFavorite}
        addToCart={addToCart}
        favorites={favorites} />} />
      </Routes>
    </BrowserRouter>
  );
}

App.propTypes = {
  cards: PropTypes.arrayOf(PropTypes.object),
  cart: PropTypes.arrayOf(PropTypes.object),
  favorites: PropTypes.arrayOf(PropTypes.object),
};

App.defaultProps = {
  cards: [],
  cart: [],
  favorites: [],
};

export default App;
