import React, { useState } from "react";
import PropTypes from "prop-types";
import Button from "./Button";
import "../scss/ProductCard.scss";
import Modal from "./Modal";

const ProductCart = ({ card, deleteFromCart, toggleFavorite, isFavorite }) => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const handleDeleteFromCart = () => {
    setIsModalOpen(true);
  };

  const handleModalClose = () => {
    setIsModalOpen(false);
    deleteFromCart(card);
  };


  return (
    <li className="card-item">
        <div className="card-item_img">
      <img
        src={card.imagePath}
        alt={card.name}
        style={{ width: "300px", height: "300px" }}
      />
      <span onClick={handleDeleteFromCart} className="card-close">&times;</span></div>
      <div className="card-item-content">
        <div className="card-item_box">
          <h2 className="card-item_title">{card.name}</h2>
          <span
            className="card-item_star"
            onClick={() => toggleFavorite(card)}
          >
            {isFavorite ? "★" : "☆"}
          </span>
        </div>
        <p>Ціна: {card.price} грн.</p>
        <p>Артикул: {card.article}</p>
        <div className="color-item">Колір: {card.color}</div>
      </div>
      <Button
        onClick={()=> {}}
        text={"✔️"}
        backgroundColor={"rgb(47, 112, 233)"}
      />
      <Modal
        header="Confirmation"
        closeButton={true}
        text={`Product "${card.name}" has been delete from the cart.`}
        isOpen={isModalOpen}
        onRequestClose={() => setIsModalOpen(false)}
        actions={
          <>
            <Button
              backgroundColor="rgb(47, 112, 233)"
              text="Ok"
              onClick={handleModalClose}
            />
            <Button
              backgroundColor="rgb(47, 112, 233)"
              text="Cancel"
              onClick={() => setIsModalOpen(false)}
            />
          </>
        }
      />
    </li>
  );
};

ProductCart.propTypes = {
  card: PropTypes.object.isRequired,
  deleteFromCart: PropTypes.func.isRequired,
  toggleFavorite: PropTypes.func.isRequired,
  isFavorite: PropTypes.bool.isRequired,
};

export default ProductCart;
