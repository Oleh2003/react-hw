
import axios from "axios";

const getData = async () => {
  const response = await axios.get("products.json");
  return response.data;
};

export default getData;