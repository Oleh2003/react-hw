import {  SET_MODAL, SET_MODAL_FORM, SET_MODAL_FORM_DELETE_CART } from "./type";

const initialState = {
  isModalOpen: false,
  selectedItemId: null,
};

export const modalReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_MODAL:
      return {
        ...state,
        isModalOpen: action.data.isOpen,
        selectedItemId: action.data.id,
      };

    case SET_MODAL_FORM:
      return {
        ...state,
        isModalFormOpen: action.data.isOpenFrom,
        selectedItemIdFrom: action.data.id,
      };
    case SET_MODAL_FORM_DELETE_CART:
      return {
        ...state,
        isModalOpen: false,
        selectedItemId: null,
      };
      

    default:
      return state;
  }
};
