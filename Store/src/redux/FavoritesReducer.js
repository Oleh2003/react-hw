import {
  ADD_FAVORITES,
  DELETE_FAVORITES,
  SAVE_FAVORITES,
  LOAD_FAVORITES,
} from "./type";

const savedFavorites = localStorage.getItem("favorites");
const initialState = savedFavorites ? JSON.parse(savedFavorites) : [];

export const favoritesReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_FAVORITES:
      return [...state, action.data];

    case SAVE_FAVORITES:
      return action.data;

    case LOAD_FAVORITES:
      return state;

    case DELETE_FAVORITES:
      return state.filter((card) => card.id !== action.data.id);

    default:
      return state;
  }
};
