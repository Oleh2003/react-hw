import Button from "./componets/Button";
import { render } from "@testing-library/react";


    test("snapshots the button" , ()=> {
        const {asFragment} = render(<Button />)
        expect(asFragment(<Button />)).toMatchSnapshot();
    })
    
    test("renders correctly", () => {
        const { container } = render(<Button text="Click me" onClick={() => {}} />);
        expect(container).toMatchSnapshot();
      });

