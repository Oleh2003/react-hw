import React from 'react'
import ProductCard from '../componets/ProductCard';
import Header from '../componets/Header';
import Footer from '../componets/layout/Footer';


function Favorites({favorites, addToCart, toggleFavorite, cart}) {
  return (
    <div className='favotites'>
       <Header cart={cart} favorites={favorites} />
    <h2 className="main-title">Featured Products For Pre-Order in your  Favorites</h2>
    <ul className="card-list">{favorites.map((card) => (
        <ProductCard
          key={card.id}
          card={card}
          isFavorite={true}
          toggleFavorite={toggleFavorite}
          addToCart={addToCart}
        />
      ))}</ul>
      <Footer />
  </div>
  );
}

export default Favorites
