import React, { useContext } from "react";
import ProductList from "../componets/ProductsList";
import Header from "../componets/Header";
import Introduction from "../componets/layout/Introduction";
import Categoties from "../componets/layout/Categories";
import CrossBorder from "../componets/layout/CrossBorder";
import Subcribe from "../componets/layout/Subcribe";
import Footer from "../componets/layout/Footer";
import ButtonCardOrTable from "../componets/ButtonCardOrTable";
import { ViewModeContext } from "../componets/ViewContextApi";
import ProductListTable from "../componets/ProductListTable";

function MainPage({ toggleFavorite, addToCart, cards, favorites, cart }) {
  const { viewMode, toggleViewMode } = useContext(ViewModeContext);
  return (
    <div className="Main-page">
      <Header cart={cart} favorites={favorites} />
      <main>
        <Introduction />
        <div className="main-box">
          <h2 className="main-title">Featured Products For Pre-Order</h2>
          <ButtonCardOrTable toggleViewMode={toggleViewMode} />
        </div>
        {viewMode === "card" ? (
          <ProductList
            toggleFavorite={toggleFavorite}
            addToCart={addToCart}
            cards={cards}
            favorites={favorites}
          />
        ) : (
          <ProductListTable
            toggleFavorite={toggleFavorite}
            addToCart={addToCart}
            cards={cards}
            favorites={favorites}
          />
        )}

        <Categoties />
        <CrossBorder />
        <Subcribe />
      </main>
      <Footer />
    </div>
  );
}

export default MainPage;
