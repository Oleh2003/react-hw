import React from "react";
import PropTypes from "prop-types";
import "../scss/ProductsList.scss";
import ProductCart from "../componets/ProductCart";
import Header from "../componets/Header";
import "../scss/Cart.scss";
import Footer from "../componets/layout/Footer";

const Cart = ({ cart, deleteFromCart, toggleFavorite, favorites }) => {
  const cartList = cart.map((card) => (
    <ProductCart
      key={card.id}
      card={card}
      deleteFromCart={deleteFromCart}
      toggleFavorite={toggleFavorite}
      isFavorite={favorites.some((p) => p.id === card.id)}
    />
  ));

  return (
    <div className="cart">
       <Header cart={cart} favorites={favorites} />
       
      <h2 className="main-title">
        Featured Products For Pre-Order in your Cart
      </h2>
      <ul className="card-list">{cartList}</ul>
      <Footer />
    </div>
  );
};

Cart.propTypes = {
  cart: PropTypes.arrayOf(PropTypes.object).isRequired,
  deleteFromCart: PropTypes.func.isRequired,
  toggleFavorite: PropTypes.func.isRequired,
  favorites: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default Cart;
