import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import getData from "./componets/ApiServer";
import { useDispatch, useSelector } from "react-redux";
import MainPage from "./pages/MainPage";
import Cart from "./pages/Cart";
import Favorites from "./pages/Favorites";
import {
  addCart,
  addFavorites,
  deleteCart,
  deleteFavorites,
  loadCart,
  loadFavorites,
  saveCart,
  saveFavorites,
} from "./redux/action";
import { ViewModeProvider } from "./componets/ViewContextApi";


function App() {
  const cards = useSelector((state) => state.dataReducer.data);
  const cart = useSelector((state) => state.cartReducer);
  const favorites = useSelector((state) => state.favoritesReducer);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getData());

    const savedCart = localStorage.getItem("cart");
    const savedFavorites = localStorage.getItem("favorites");

    if (savedCart) {
      dispatch(loadCart(savedCart));
    }

    if (savedFavorites) {
      dispatch(loadFavorites(savedFavorites));
    }
  }, [dispatch]);

  useEffect(() => {
    saveCart(cart);
    saveFavorites(favorites);
  }, [cart, favorites]);

  const addToCart = (product) => {
    dispatch(addCart(product));
  };

  const addToFavorites = (product) => {
    dispatch(addFavorites(product));
  };

  const removeFromFavorites = (card) => {
    dispatch(deleteFavorites(card));
  };

  const toggleFavorite = (product) => {
    if (favorites.find((p) => p.id === product.id)) {
      removeFromFavorites(product);
    } else {
      addToFavorites(product);
    }
  };

  const deleteFromCart = (product) => {
    dispatch(deleteCart(product));
  };

  return (
    <BrowserRouter >
      <div className="container">
      <Routes>
        <Route
          path="/"
          element={
            <ViewModeProvider>
            <MainPage
              toggleFavorite={toggleFavorite}
              addToCart={addToCart}
              cards={cards}
              favorites={favorites}
              cart={cart}
            /></ViewModeProvider>
          }
        />
        <Route
          path="/cart"
          element={
            <Cart
              toggleFavorite={toggleFavorite}
              deleteFromCart={deleteFromCart}
              cart={cart}
              favorites={favorites}
            />
          }
        />
        <Route
          path="/favorites"
          element={
            <Favorites
              toggleFavorite={toggleFavorite}
              addToCart={addToCart}
              favorites={favorites}
              cart={cart}
            />
          }
        />
      </Routes></div>
    </BrowserRouter>
  );
}

App.propTypes = {
  cards: PropTypes.arrayOf(PropTypes.object),
  cart: PropTypes.arrayOf(PropTypes.object),
  favorites: PropTypes.arrayOf(PropTypes.object),
};

App.defaultProps = {
  cards: [],
  cart: [],
  favorites: [],
};

export default App;
