import React from "react";
import facebook from "../../image/facebook.svg";
import instagram from "../../image/instagram.svg";
import twitter from "../../image/twitter.svg";
import linkedIn from "../../image/linkedIn.svg";
import { Link } from "react-router-dom";

function Footer() {
  return (
    <div className="line-footer">
      <footer className="footer">
        <div className="footer_contacts contacts">
          <h2 className="contacts_title">Contacts</h2>
          <div className="contacts_text">
            <p>4517 Washington Ave. Manchester, Kentucky 39495</p>
          </div>
          <a className="contacts_tel" href="tel:(+239) 555-0108">
            (+239) 555-0108
          </a>
          <a className="contacts_email" href="mailto:contact@agon.com">
            contact@agon.com
          </a>
          <div className="contacts_icon">
            <img src={facebook} alt="icon" />
            <img src={instagram} alt="icon" />
            <img src={twitter} alt="icon" />
            <img src={linkedIn} alt="icon" />
          </div>
        </div>
        <div className="footer_content content">
          <h2 className="content_title">About Us</h2>
          <ul className="content_list">
            <li className="content_item">
              <Link className="content_link" to="/">
                About V1
              </Link>
            </li>
            <li className="content_item">
              <Link className="content_link" to="/">
                About V2
              </Link>
            </li>
            <li className="content_item">
              <Link className="content_link" to="/">
                About V3
              </Link>
            </li>
            <li className="content_item">
              <Link className="content_link" to="/">
                Services V1
              </Link>
            </li>
            <li className="content_item">
              <Link className="content_link" to="/">
                Services V2
              </Link>
            </li>
          </ul>
        </div>
        <div className="footer_content content">
          <h2 className="content_title">Discover</h2>
          <ul className="content_list">
            <li className="content_item">
              <Link className="content_link" to="/">
                Our Blog V1
              </Link>
            </li>
            <li className="content_item">
              <Link className="content_link" to="/">
                Our Blog V2
              </Link>
            </li>
            <li className="content_item">
              <Link className="content_link" to="/">
                Our Blog V3
              </Link>
            </li>
            <li className="content_item">
              <Link className="content_link" to="/">
                Blog Single{" "}
              </Link>
            </li>
            <li className="content_item">
              <Link className="content_link" to="/">
                Page 404
              </Link>
            </li>
          </ul>
        </div>
        <div className="footer_content content">
          <h2 className="content_title">Support </h2>
          <ul className="content_list">
            <li className="content_item">
              <Link className="content_link" to="/">
                FAQs V1
              </Link>
            </li>
            <li className="content_item">
              <Link className="content_link" to="/">
                FAQs V2
              </Link>
            </li>
            <li className="content_item">
              <Link className="content_link" to="/">
                FAQs V3
              </Link>
            </li>
            <li className="content_item">
              <Link className="content_link" to="/">
                Career Detail
              </Link>
            </li>
            <li className="content_item">
              <Link className="content_link" to="/">
                Contact Us
              </Link>
            </li>
          </ul>
        </div>
        <div className="footer_content content">
          <h2 className="content_title">Useful links</h2>
          <ul className="content_list">
            <li className="content_item">
              <Link className="content_link" to="/">
                Shop V1
              </Link>
            </li>
            <li className="content_item">
              <Link className="content_link" to="/">
                Shop V2
              </Link>
            </li>
            <li className="content_item">
              <Link className="content_link" to="/">
                Shop V3
              </Link>
            </li>
            <li className="content_item">
              <Link className="content_link" to="/">
                Pricing{" "}
              </Link>
            </li>
            <li className="content_item">
              <Link className="content_link" to="/">
                Terms & Conditions{" "}
              </Link>
            </li>
          </ul>
        </div>
      </footer>
      <div className="text">
        <h2 className="text_title">©Official 2023</h2>
        <span>Privacy policy Cookies Terms of service</span>
      </div>
    </div>
  );
}

export default Footer;
