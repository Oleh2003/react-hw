import React from "react";
import crossBorder1 from "../../image/cross-border-1.svg";
import crossBorder2 from "../../image/cross-border-2.svg";
import crossBorder3 from "../../image/cross-border-3.svg";
import crossBorder4 from "../../image/cross-border-4.svg";
import crossBorder5 from "../../image/cross-border-5.svg";
import crossBorderArrow1 from "../../image/cross-border-arrow.svg";
import crossBorderArrow2 from "../../image/cross-border-arrow-2.svg";
import crossBorderArrow3 from "../../image/cross-border-arrow-3.svg";

function CrossBorder() {
  return (
    <div className="cross-border">
      <h2 className="main-title">
        Cross-border ordering has never been easier.
      </h2>
      <div className="cross-border_content">
        <div className="cross-border_block block-1">
          <div className="cross-border_img">
            <img src={crossBorder1} alt="icon" />
          </div>
          <h2 className="cross-border_title">Start Plan</h2>
          <div className="cross-border_text">
            <p>Choose any of our packages</p>
          </div>
          <span>
            {" "}
            <img src={crossBorderArrow1} alt="arrow" />
          </span>
        </div>

        <div className="cross-border_block block-2">
          <div className="cross-border_img">
            <img src={crossBorder2} alt="icon" />
          </div>
          <h2 className="cross-border_title">Connect</h2>
          <div className="cross-border_text">
            <p>Receive concepts In 24 hours</p>
          </div>
          <span>
            {" "}
            <img src={crossBorderArrow1} alt="arrow" />
          </span>
        </div>
        <div className="cross-border_block block-3">
          <div className="cross-border_img">
            <img src={crossBorder3} alt="icon" />
          </div>
          <h2 className="cross-border_title">Match</h2>
          <div className="cross-border_text">
            <p>Development Stage</p>
          </div>
          <span>
            {" "}
            <img src={crossBorderArrow1} alt="arrow" />
          </span>
        </div>
        <div className="cross-border_block block-4">
          <div className="cross-border_img">
            <img src={crossBorder4} alt="icon" />
          </div>
          <h2 className="cross-border_title">Complete</h2>
          <div className="cross-border_text">
            <p>After-release Support</p>
          </div>
          <span>
            {" "}
            <img src={crossBorderArrow3} alt="arrow" />
          </span>
        </div>
        <div className="cross-border_block block-5">
          <div className="cross-border_img">
            <img src={crossBorder5} alt="icon" />
          </div>
          <h2 className="cross-border_title">Review</h2>
          <div className="cross-border_text">
            <p>Project launch and checkout</p>
          </div>
          <span>
            {" "}
            <img src={crossBorderArrow3} alt="arrow" />
          </span>
        </div>
        <span>
          {" "}
          <img src={crossBorderArrow2} alt="arrow" />
        </span>
      </div>
    </div>
  );
}

export default CrossBorder;
