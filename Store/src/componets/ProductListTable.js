import React from "react";
import PropTypes from "prop-types";
import ProductTableItem from "./ProductTableItem";
import "../scss/ProductListTable.scss"

const ProductListTable = ({ cards, addToCart, toggleFavorite, favorites }) => {
  const tableRows = cards.map((card) => (
    <ProductTableItem
      key={card.id}
      card={card}
      addToCart={addToCart}
      toggleFavorite={toggleFavorite}
      isFavorite={favorites.some((p) => p.id === card.id)}
    />
  ));

  return (
    <table className="table">
      <tbody className="table_list">{tableRows}</tbody>
    </table>
  );
};

ProductListTable.propTypes = {
  cards: PropTypes.arrayOf(PropTypes.object).isRequired,
  addToCart: PropTypes.func.isRequired,
  toggleFavorite: PropTypes.func.isRequired,
  favorites: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default ProductListTable;
