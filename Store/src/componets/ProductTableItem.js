import React from "react";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import Button from "./Button";
import Modal from "./Modal";
import { setModal } from "../redux/action";
import "../scss/ProductTableItem.scss";

const ProductTableItem = ({ card, addToCart, toggleFavorite, isFavorite }) => {
  const isModalOpen = useSelector((state) => state.modalReducer.isModalOpen);
  const selectedItemId = useSelector(
    (state) => state.modalReducer.selectedItemId
  );

  const dispatch = useDispatch();

  const handleAddToCart = () => {
    dispatch(setModal(true, card.id));
  };

  const handleModalClose = () => {
    dispatch(setModal(false, card.id));
    addToCart(card);
  };

  const ModalClose = () => {
    dispatch(setModal(false, card.id));
  };

  return (
    <tr className="table-item">
      <td className="card-item_img">
        <img src={card.imagePath} alt={card.name} />
      </td>
      <td className="table-item_box">
        <h2 className="card-item_title">{card.name}</h2>
        <span className="card-item_star" onClick={() => toggleFavorite(card)}>
          {isFavorite ? "★" : "☆"}
        </span>
      </td>
      <td className="table-item_flex">
        <p className="card-item_price">{card.price}</p>
        <p className="card-item_color">Color: {card.color}</p>
      </td>
      <td className="table-item_btn">
      <Button
        onClick={handleAddToCart}
        text={"Add to cart"}
        backgroundColor={"rgba(190, 225, 230, 1)"}
      />
      </td>
      
      <Modal
        header="Confirmation"
        closeButton={true}
        text={`Product  has been added to the cart.`}
        isOpen={isModalOpen && selectedItemId === card.id}
        onRequestClose={ModalClose}
        actions={
          <>
            <Button
              backgroundColor="rgb(47, 112, 233)"
              text="OK"
              onClick={handleModalClose}
            />
            <Button
              backgroundColor="rgb(47, 112, 233)"
              text="CLOSE"
              onClick={ModalClose}
            />
          </>
        }
      />
    </tr>
  );
};

ProductTableItem.propTypes = {
  card: PropTypes.object.isRequired,
  addToCart: PropTypes.func.isRequired,
  toggleFavorite: PropTypes.func.isRequired,
  isFavorite: PropTypes.bool.isRequired,
};

export default ProductTableItem;
