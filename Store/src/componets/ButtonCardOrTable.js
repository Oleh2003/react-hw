import React, {useContext} from 'react'
import { ViewModeContext } from './ViewContextApi'

function ButtonCardOrTable() {
    const { toggleViewMode } = useContext(ViewModeContext);
  return < button className='button main-btn' onClick={toggleViewMode}> Card / Table </button>
}

export default ButtonCardOrTable
