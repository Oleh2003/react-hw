import  React  from "react";
import PropTypes from "prop-types";
import "../scss/Button.scss";

function Button ({ backgroundColor, text, onClick }) {

    return (
      <div className="button-box">
      <button className="btn" style={{ backgroundColor }} onClick={onClick}>
        {text}
      </button></div>
    );
  }


Button.propTypes = {
  backgroundColor: PropTypes.string,
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default Button;