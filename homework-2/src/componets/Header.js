import { Component } from "react";
import PropTypes from "prop-types";
import "../scss/Header.scss";


class Header extends Component {
  render() {
    const { title, cart, favorites } = this.props;

    return (
      <div className="header">
        <h2 className="header_title">
          {title}
        </h2>
        <div className="header_btn-box">
          <button className="header_btn" onClick={() => {}}>
            Обране {favorites.length}
            &#9734;
          </button>
          <button className="header_btn" onClick={() => {}}>
            Кошик {cart.length}
            &#128722;
          </button>
        </div>
      </div>
    );
  }
}

Header.propTypes = {
  title: PropTypes.string.isRequired,
  cart: PropTypes.arrayOf(PropTypes.object),
  favorites: PropTypes.arrayOf(PropTypes.object),
};

Header.defaultProps = {
  cart: [],
  favorites: [],
};

export default Header;