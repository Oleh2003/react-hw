import { Component } from "react";
import PropTypes from "prop-types";
import Button from "./Button";
import "../scss/ProductCard.scss";
import Modal from "./Modal";

class ProductCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalOpen: false,
    };
  }

  handleAddToCart = () => {
    this.setState({ isModalOpen: true });
  };

  handleModalClose = () => {
    this.setState({ isModalOpen: false });
    this.props.addToCart(this.props.card);
  };

  render() {
    const { card, toggleFavorite, isFavorite } = this.props;
    const { isModalOpen } = this.state;

    return (
      <li className="card-item">
        <img src={card.imagePath} alt={card.name}
        style={{width: '300px', height: '300px'}} />
        <div className="card-item-content">
            <div className="card-item_box">
            <h2 className="card-item_title">{card.name}</h2>
          <span className="card-item_star" onClick={() => toggleFavorite(card)}>
            {isFavorite ? "★" : "☆"}
          </span>
            </div>
          <p>Ціна: {card.price} грн.</p>
          <p>Артикул: {card.article}</p>
          <div className="color-item">
            Колір:{card.color}
          </div>
        </div>
        <Button
          onClick={this.handleAddToCart}
          text={"Add to cart"}
          backgroundColor={"rgb(47, 112, 233)"}
        />
        <Modal
          header="Confirmation"
          closeButton={true}
          text={`Product "${card.name}" has been added to the cart.`}
          isOpen={isModalOpen}
          onRequestClose={() => this.setState({ isModalOpen: false })}
          actions={
            <>
              <Button
                backgroundColor="rgb(47, 112, 233)"
                text="Ok"
                onClick={this.handleModalClose}
              />
              <Button
                backgroundColor="rgb(47, 112, 233)"
                text="Cancel"
                onClick={() => this.setState({ isModalOpen: false })}
              />
            </>
          }
        />
      </li>
    );
  }
}

ProductCard.propTypes = {
  card: PropTypes.object.isRequired,
  addToCart: PropTypes.func.isRequired,
  toggleFavorite: PropTypes.func.isRequired,
  isFavorite: PropTypes.bool.isRequired,
};

export default ProductCard;