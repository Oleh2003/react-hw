import { Component } from "react";
import "./scss/App.scss";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./pages/MainPage";
import Cart from "./pages/Cart";
import Favorites from "./pages/Favorites";



class App extends Component {

  render() {
    return(
      <BrowserRouter>
      <Routes>
        <Route path="/" element={<MainPage/>} />
        <Route path="/cart" element={<Cart/>} />
        <Route path="/favorites" element={<Favorites/>} />
      </Routes>
    </BrowserRouter>
    )
    
  }
}



export default App;